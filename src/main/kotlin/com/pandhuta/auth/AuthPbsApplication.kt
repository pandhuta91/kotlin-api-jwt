package com.pandhuta.auth

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.security.web.SecurityFilterChain

@SpringBootApplication(exclude = [SecurityAutoConfiguration::class])
class AuthPbsApplication

fun main(args: Array<String>) {
	runApplication<AuthPbsApplication>(*args)
}
